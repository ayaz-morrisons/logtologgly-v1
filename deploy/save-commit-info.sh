commit_hash=$(git log -1 --pretty=format:"%h")
committer=$(git log -1 --pretty=format:"%ce")
commit_date=$(git log -1 --pretty=format:"%cd")
tags=$(git tag --contains HEAD)

echo "<SharedFlowBundle>" > commit-info
echo "	<Description>commit ${commit_hash} by ${committer} on ${commit_date} ${tags}</Description>" >> commit-info
echo "</SharedFlowBundle>" >> commit-info

cat commit-info