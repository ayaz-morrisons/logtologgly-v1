#!/bin/bash
username=$1
password=$2
env=$3
sharedflowname=$4
orgname=$5
env_dev=\"dev\",
env_cit=\"cit\",
env_sit=\"sit\",
env_uat=\"uat\",
env_pre=\"pre\",
env_tst=\"tst\",
env_prd=\"prd\",
deployenv=""
deployed_rev=""

declare -a revArr
# Get the list of revisions of the API
while read line ;do
	
	if [[ $line =~ \"revision\" ]];then
		set $line
		revValue=$line
		echo  " ***********************************line is  $line End"
		revValue=${revValue##*[}
		revValue=${revValue%%]*}
		i=0
		
		while [ -n "$revValue" ] ;do
			value=${revValue%%,*}
			#trim whitespaces
			value=${value%% }
			value=${value## }
			value+=,
			revArr[i]=$value
			((i++))
			valueLen=${#value}
			revValue=${revValue:$((valueLen+1))}
		done
	fi
done < <(curl -u $username:$password -X GET "https://api.enterprise.apigee.com/v1/organizations/$orgname/sharedflows/$sharedflowname")

# Get deployments
while read line ;do
	if [[ $line =~ \"name\"  ]];then
		set $line
		echo  " ***********************************line is  $1 $2 $3 End"

		if [[ $deployenv = $env_dev ]];then
			echo "Setting dev revision...."
			post_dev_revision=$3
			deployenv=""
		fi
		if [[ $deployenv = $env_cit ]];then
			echo "Setting cit revision...."
			post_cit_revision=$3
			deployenv=""
		fi
		if [[ $deployenv = $env_sit ]];then
			echo "Setting sit revision...."
			post_sit_revision=$3
			deployenv=""
		fi
		if [[ $deployenv = $env_uat ]];then
			echo "Setting uat revision...."
			post_uat_revision=$3
			deployenv=""
		fi
		if [[ $deployenv = $env_pre ]];then
			echo "Setting pre revision...."
			post_pre_revision=$3
			deployenv=""
		fi
		if [[ $deployenv = $env_tst ]];then
			echo "Setting tst revision...."
			post_tst_revision=$3
			deployenv=""
		fi
		if [[ $deployenv = $env_prd ]];then
			echo "Setting prd revision...."
			post_prd_revision=$3
			deployenv=""
		fi
		if [[ $3 = $env_dev ]];then
			deployenv=$env_dev
		fi
		if [[ $3 = $env_cit ]];then
			deployenv=$env_cit
		fi
		if [[ $3 = $env_sit ]];then
			deployenv=$env_sit
		fi
		if [[ $3 = $env_uat ]];then
			deployenv=$env_uat
		fi
		if [[ $3 = $env_pre ]];then
			deployenv=$env_pre
		fi
		if [[ $3 = $env_tst ]];then
			deployenv=$env_tst
		fi
		if [[ $3 = $env_prd ]];then
			deployenv=$env_prd
		fi
	fi
done < <(curl -u $username:$password -X GET "https://api.enterprise.apigee.com/v1/organizations/$orgname/sharedflows/$sharedflowname/deployments")

if [[ $env = "dev" ]];then
	deployed_rev=$post_dev_revision
elif [[ $env = "cit" ]];then
	deployed_rev=$post_cit_revision
elif [[ $env = "sit" ]];then
	deployed_rev=$post_sit_revision
elif [[ $env = "uat" ]];then
	deployed_rev=$post_uat_revision
elif [[ $env = "pre" ]];then
	deployed_rev=$post_pre_revision
elif [[ $env = "tst" ]];then
	deployed_rev=$post_tst_revision
elif [[ $env = "prd" ]];then
	deployed_rev=$post_prd_revision
else
	echo "Invalid environment $env"
fi

deployed_rev=$(echo $deployed_rev | sed 's/"//g' | sed 's/,//g')

echo curl -X PUT -d @commit-info "https://api.enterprise.apigee.com/v1/organizations/$orgname/sharedflows/$sharedflowname/revisions/${deployed_rev}" -H "Content-Type: application/xml" -u $username:$password
curl -X PUT -d @commit-info "https://api.enterprise.apigee.com/v1/organizations/$orgname/sharedflows/$sharedflowname/revisions/${deployed_rev}" -H "Content-Type: application/xml" -u $username:$password
