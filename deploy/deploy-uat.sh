#!/bin/bash

username=$1
password=$2
sharedflowname=$3
overridedelay=$4
env_dev=\"dev\",
env_cit=\"cit\",
env_sit=\"sit\",
env_uat=\"uat\",
env_pre=\"pre\",
deployenv=""
sit_revision=""
uat_revision=""

while read line ;do
	if [[ $line =~ \"name\"  ]];then
		set $line
		echo  " ***********************************line is  $1 $2 $3 End"

		if [[ $deployenv == $env_sit ]];then
			echo "Setting sit revision...."
			sit_revision=$3
			deployenv=""
		fi
		if [[ $deployenv == $env_uat ]];then
			echo "Setting uat revision...."
			uat_revision=$3
			deployenv=""
		fi
		
		if [[ $3 == $env_sit ]];then
			deployenv=$env_sit
		fi
		if [[ $3 == $env_uat ]];then
			deployenv=$env_uat
		fi
		echo "Results Deployment Env: $deployenv sit Revision: $sit_revision uat Revision: $uat_revision"
	fi
done < <(curl -u $username:$password -X GET "https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/sharedflows/$sharedflowname/deployments")

sit_revision_length=${#sit_revision}
sit_revision_length=$((sit_revision_length - 3))
sit_revision_num=${sit_revision:1:$sit_revision_length}
echo "sit_revision: $sit_revision_num"
uat_revision_length=${#uat_revision}
uat_revision_length=$((uat_revision_length - 3))
uat_revision_num=${uat_revision:1:$uat_revision_length}
echo "uat_revision: $uat_revision_num"

# Undeploy in UAT
curl -u $username:$password -X DELETE https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/environments/uat/sharedflows/$sharedflowname/revisions/$uat_revision_num/deployments
# Deploy in UAT
echo "curl -u $username:***** -X POST https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/environments/uat/sharedflows/$sharedflowname/revisions/$sit_revision_num/deployments?override=true&delay=$overridedelay"
curl -u $username:$password -X POST https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/environments/uat/sharedflows/$sharedflowname/revisions/$sit_revision_num/deployments?override=true&delay=$overridedelay

