#!/bin/bash

username=$1
password=$2
sharedflowname=$3
overridedelay=$4
env_tst=\"tst\",
env_prd=\"prd\",
deployenv=""
tst_revision=""
prd_revision=""


while read line ;do
	if [[ $line =~ \"name\"  ]];then
		set $line
		echo  " ***********************************line is  $1 $2 $3 End"

		if [ $deployenv = $env_tst ];then
			echo "Setting tst revision...."
			tst_revision=$3
			deployenv=""
		fi
		if [ $deployenv = $env_prd ];then
			echo "Setting prd revision...."
			prd_revision=$3
			deployenv=""
		fi
		if [ $3 =  $env_tst ];then
			deployenv=$env_tst
		fi
		if [ $3 =  $env_prd ];then
			deployenv=$env_prd
		fi
		echo "Results Deployment Env: $deployenv TST Revision: $tst_revision PRD Revision: $prd_revision"
	fi
done < <(curl -u $username:$password -X GET "https://api.enterprise.apigee.com/v1/organizations/morrisons-pci/sharedflows/$sharedflowname/deployments")

tst_revision_length=${#tst_revision}
tst_revision_length=$((tst_revision_length - 3))
tst_revision_num=${tst_revision:1:$tst_revision_length}
echo "tst_revision: $tst_revision_num"
prd_revision_length=${#prd_revision}
prd_revision_length=$((prd_revision_length - 3))
prd_revision_num=${prd_revision:1:$prd_revision_length}
echo "prd_revision: $prd_revision_num"

# Undeploy in PRD
curl -u $username:$password -X DELETE https://api.enterprise.apigee.com/v1/organizations/morrisons-pci/environments/prd/sharedflows/$sharedflowname/revisions/$prd_revision_num/deployments
# Deploy in PRD
echo "curl -u $username:**** -X POST https://api.enterprise.apigee.com/v1/organizations/morrisons-pci/environments/prd/sharedflows/$sharedflowname/revisions/$tst_revision_num/deployments?override=true&delay=$overridedelay"
curl -u $username:$password -X POST https://api.enterprise.apigee.com/v1/organizations/morrisons-pci/environments/prd/sharedflows/$sharedflowname/revisions/$tst_revision_num/deployments?override=true&delay=$overridedelay
